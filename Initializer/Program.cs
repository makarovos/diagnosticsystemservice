﻿namespace Initializer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public partial class Program
    {
        private static readonly Dictionary<string, Action> options =
            new Dictionary<string, Action>
            {
                ["Скомпилировать скетчи для микроконтроллеров"] = CompileSkethes,
                ["Имитация работы микроконтроллеров"] = SimulateMicrocontrollers,
            };
        
        public static void Main(string[] args)
        {
            Console.WriteLine("Выберите пункт для выполнения:");
            var optionsByIndex = options.Select((entry, i) =>
                {
                    Console.WriteLine($"{i}) {entry.Key}");
                    return entry.Value;
                })
                .ToArray();

            try
            {
                var index = int.Parse(args?.FirstOrDefault() ?? Console.ReadLine());
                optionsByIndex[index]();
                Console.WriteLine("Successfully done");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }

            Console.WriteLine();
            Console.WriteLine("Press Enter for exit...");
            Console.ReadLine();
        }

        private static void Log(string log = null)
        {
            Console.WriteLine(log);
        }
    }
}
