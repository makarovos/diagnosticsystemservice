﻿namespace Initializer
{
    using System;
    using System.IO;
    using Models;

    public partial class Program
    {
        private static string ssid = Constants.Ssid;
        private static string password = Constants.SsidPassword;
        private static string host = Constants.Host;
        private static string port = Constants.Port;
        private static uint delay = Constants.MicroControllerDelay;

        private static string folder = @"C:\Users\user\Desktop\sketches";
        private static uint timeoutDelay = Constants.MicroControllerServerWaitingDelay;

        private static void CompileSkethes()
        {
            Console.Write($"Путь к папке, где будут лежать скетчи ({folder}): ");
            var input = Console.ReadLine();
            folder = !string.IsNullOrEmpty(input) ? input.Trim() : folder;

            Console.Write($"SSID Wi-Fi сети ({ssid}): ");
            input = Console.ReadLine();
            ssid = !string.IsNullOrEmpty(input) ? input.Trim() : ssid;

            Console.Write($"Пароль Wi-Fi сети ({password}): ");
            input = Console.ReadLine();
            password = !string.IsNullOrEmpty(input) ? input.Trim() : password;

            Console.Write($"Хост сервера системы диагностики ({host}): ");
            input = Console.ReadLine();
            host = !string.IsNullOrEmpty(input) ? input.Trim() : host;

            Console.Write($"Порт сервера системы диагностики ({port}): ");
            input = Console.ReadLine();
            port = !string.IsNullOrEmpty(input) ? input.Trim() : port;

            Console.Write($"Задержка микроконтроллера ({delay}): ");
            input = Console.ReadLine();
            delay = !string.IsNullOrEmpty(input) ? uint.Parse(input.Trim()) : delay;

            Console.Write($"Максимальный таймаут сервера системы диагностики сети ({timeoutDelay}): ");
            input = Console.ReadLine();
            timeoutDelay = !string.IsNullOrEmpty(input) ? uint.Parse(input.Trim()) : timeoutDelay;

            BuildSketch(SensorType.Temperature1);
            BuildSketch(SensorType.Temperature2);
            BuildSketch(SensorType.Temperature3);
            BuildSketch(SensorType.Pressure1);
            BuildSketch(SensorType.Pressure2);
        }

        private static void BuildSketch(SensorType type)
        {
            var directory = Path.Combine(folder, type.ToString());
            Directory.CreateDirectory(directory);
            File.WriteAllText(Path.Combine(directory, $"{type}.ino"), GenerateSketch(type));
        }
        
        private static string GenerateSketch(SensorType type)
        {
            string topSnippet;
            switch (type)
            {
                case SensorType.Temperature1:
                case SensorType.Temperature2:
                case SensorType.Temperature3:
                    topSnippet = @"# include <OneWire.h>
# include <DallasTemperature.h>
OneWire oneWire(D2);
DallasTemperature sensors(
&oneWire);

float getData()
{
    sensors.requestTemperatures();
    return sensors.getTempCByIndex(0);
}";
                    break;
                case SensorType.Pressure1:
                case SensorType.Pressure2:
                    topSnippet = @"float getData()
{
    return 0;
}";
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }

            return $@"#include <ESP8266WiFi.h>

{topSnippet}

void setup()
{{
    Serial.begin(115200);
    delay(10);
    sensors.begin();
    WiFi.mode(WIFI_STA);
    WiFi.begin(""{ssid}"", ""{password}"");
    while (WiFi.status() != WL_CONNECTED)
    {{
        delay(200);
        Serial.print(""."");
    }}
}}

void loop()
{{
    WiFiClient client;
    if (!client.connect(""{host}"", {port}))
    {{
        Serial.println(""connection failed"");
        return;
    }}

    client.print(""POST /data/create?type={(int) type}&value="" + String(getData()) + "" HTTP/1.1\r\n"" +
                    ""Host: {host}\r\n"" +
                    ""Connection: close\r\n\r\n"");

    unsigned long timeout = millis();
    while (client.available() == 0)
    {{
        if (millis() - timeout > {timeoutDelay})
        {{
            Serial.println(""Timeout is expired!"");
            client.stop();
            return;
        }}
    }}

    while (client.available())
    {{
        Serial.print(client.readStringUntil('\r'));
    }}

    Serial.println();
    Serial.println();

    delay({delay});
}}
";
        }
    }
}
