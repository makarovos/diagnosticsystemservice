﻿namespace Initializer
{
    using System;
    using System.Globalization;
    using System.Net;
    using System.Threading.Tasks;
    using Models;

    public partial class Program
    {
        public static void SimulateMicrocontrollers()
        {
            Task.Factory.StartNew(async () => await DDoS(SensorType.Temperature1, 10, 50));
            Task.Factory.StartNew(async () => await DDoS(SensorType.Temperature2, 10, 50));
            Task.Factory.StartNew(async () => await DDoS(SensorType.Temperature3, 10, 50));
            Task.Factory.StartNew(async () => await DDoS(SensorType.Pressure1, 1, 7));
            Task.Factory.StartNew(async () => await DDoS(SensorType.Pressure2, 1, 7));
        }

        private static async Task DDoS(SensorType type, double min, double max)
        {
            await Task.Delay(500);
            var wc = new WebClient();
            var r = new Random((int) (DateTime.Now.Ticks % 1000000));
            while (true)
            {
                var value = (min + Math.Abs(max - min) * r.NextDouble()).ToString("F1", CultureInfo.InvariantCulture);
                var uri = $"http://{Constants.ServerIpAddress}/data/create?type={(int) type}&value={value}";
                try
                {
                    var result = await wc.DownloadStringTaskAsync(uri);
                    Console.WriteLine($"{type} {value} {result}");
                }
                catch (Exception exc)
                {
                    Console.WriteLine($"{uri} {exc.GetType()}");
                }

                await Task.Delay(500);
            }
        }
    }
}
