﻿// TopShelfService.cs
namespace DiagnosticSystemService
{
    using System;
    using System.Globalization;
    using System.Net.Http.Formatting;
    using System.Web.Http;
    using Microsoft.Owin.Hosting;
    using Owin;
    using Models;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    public class TopShelfService
    {
        private IDisposable _webApp;

        public void Start()
        {
            _webApp = WebApp.Start<Startup>($"http://+:{Constants.Port}");
        }

        public void Stop()
        {
            _webApp.Dispose();
        }
    }

    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();

            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());
            config.Formatters.JsonFormatter.SerializerSettings = new JsonSerializerSettings
            {
                Culture = CultureInfo.InvariantCulture,
                Formatting = Formatting.Indented,
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
            
            app.UseWebApi(config);
        }
    }
}
