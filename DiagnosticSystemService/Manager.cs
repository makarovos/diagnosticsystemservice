﻿// Manager.cs
namespace DiagnosticSystemService
{
    using System;
    using System.Collections.Concurrent;
    using Models;
    using static Models.Constants;

    public static class Manager
    {
        private static readonly ConcurrentDictionary<SensorType, SensorData> Dictionary;

        static Manager()
        {
            Dictionary = new ConcurrentDictionary<SensorType, SensorData>
            {
                [SensorType.Temperature1] = null,
                [SensorType.Temperature2] = null,
                [SensorType.Temperature3] = null,
                [SensorType.Pressure1] = null,
                [SensorType.Pressure2] = null
            };
        }

        public static void Update(SensorType type, double value)
        {
            Dictionary[type] = new SensorData(value);
        }
        
        public static DiagnosticSystemState Last()
        {
            var expirationTime = DateTime.UtcNow.Subtract(SensorDataExpirationTimeSpan);
            
            var temperature1 = Dictionary[SensorType.Temperature1];
            var temperature2 = Dictionary[SensorType.Temperature2];
            var temperature3 = Dictionary[SensorType.Temperature3];
            var pressure1 = Dictionary[SensorType.Pressure1];
            var pressure2 = Dictionary[SensorType.Pressure2];

            return new DiagnosticSystemState
            {
                Temp1 = Temp1Result(temperature1, expirationTime),
                Temp2 = Temp2Result(temperature2, expirationTime),
                Temp3 = Temp3Result(temperature2, temperature3, expirationTime),
                Press1 = Press1Result(pressure1, expirationTime),
                Press2 = Press2Result(pressure1, pressure2, expirationTime)
            };
        }

        private static SensorDataResult Temp1Result(SensorData temp1, DateTime expirationDateTime)
        {
            if (temp1 == null || temp1.DateTime < expirationDateTime)
            {
                return new SensorDataResult(ResultType.Error, null, "Отказ микроконтроллера");
            }

            if (temp1.Value > Temp1OptimalEnd)
            {
                return new SensorDataResult(ResultType.Error, temp1.Value, "Превышение температуры");
            }

            if (temp1.Value > Temp1MaxWarning)
            {
                return new SensorDataResult(ResultType.Warning, temp1.Value, "Возможно превышение температуры");
            }

            return new SensorDataResult(ResultType.Success, temp1.Value, "Показатели в норме");
        }

        private static SensorDataResult Temp2Result(SensorData temp2, DateTime expirationDateTime)
        {
            if (temp2 == null || temp2.DateTime < expirationDateTime)
            {
                return new SensorDataResult(ResultType.Error, null, "Отказ микроконтроллера");
            }

            if (temp2.Value > Temp2OptimalEnd)
            {
                return new SensorDataResult(ResultType.Warning, temp2.Value, "Превышение температуры");
            }
            
            return new SensorDataResult(ResultType.Success, temp2.Value, "Показатели в норме");
        }

        private static SensorDataResult Temp3Result(SensorData temp2, SensorData temp3, DateTime expirationDateTime)
        {
            if (temp3 == null || temp3.DateTime < expirationDateTime)
            {
                return new SensorDataResult(ResultType.Error, null, "Отказ микроконтроллера");
            }

            if (temp2 == null || temp2.DateTime < expirationDateTime)
            {
                return new SensorDataResult(ResultType.Warning, temp3.Value, "Эффективность охлаждения не определена");
            }

            if (temp3.Value > temp2.Value - CoolingEffectiveDifference)
            {
                return new SensorDataResult(ResultType.Warning, temp3.Value, "Низкая эффективность охлаждения");
            }
            
            return new SensorDataResult(ResultType.Success, temp3.Value, "Показатели в норме");
        }

        private static SensorDataResult Press1Result(SensorData pressure1, DateTime expirationDateTime)
        {
            if (pressure1 == null || pressure1.DateTime < expirationDateTime)
            {
                return new SensorDataResult(ResultType.Error, null, "Отказ микроконтроллера");
            }

            if (pressure1.Value > Press1OptimalEnd)
            {
                return new SensorDataResult(ResultType.Error, pressure1.Value, "Превышение давления");
            }

            if (pressure1.Value < Press1OptimalStart)
            {
                return new SensorDataResult(ResultType.Error, pressure1.Value, "Падение давления");
            }

            if (pressure1.Value > Press1MaxWarning)
            {
                return new SensorDataResult(ResultType.Warning, pressure1.Value, "Возможно превышение давления");
            }

            if (pressure1.Value < Press1MinWarning)
            {
                return new SensorDataResult(ResultType.Warning, pressure1.Value, "Возможно падение давления");
            }

            return new SensorDataResult(ResultType.Success, pressure1.Value, "Показатели в норме");
        }

        private static SensorDataResult Press2Result(SensorData pressure1, SensorData pressure2, DateTime expirationDateTime)
        {
            if (pressure2 == null || pressure2.DateTime < expirationDateTime)
            {
                return new SensorDataResult(ResultType.Error, null, "Отказ микроконтроллера");
            }

            if (pressure2.Value > Press2OptimalEnd)
            {
                return new SensorDataResult(ResultType.Error, pressure2.Value, "Превышение давления");
            }

            if (pressure2.Value < Press2OptimalStart)
            {
                return new SensorDataResult(ResultType.Error, pressure2.Value, "Падение давления");
            }

            if (pressure2.Value > Press2MaxWarning)
            {
                return new SensorDataResult(ResultType.Warning, pressure2.Value, "Возможно превышение давления");
            }

            if (pressure2.Value < Press2MinWarning)
            {
                return new SensorDataResult(ResultType.Warning, pressure2.Value, "Возможно падение давления");
            }

            if (pressure1 != null && pressure1.DateTime < expirationDateTime && pressure1.Value < pressure2.Value + MaxAllowedDifferenceBetweenPressures)
            {
                return new SensorDataResult(ResultType.Warning, pressure1.Value, "Засорение фильтра");
            }

            return new SensorDataResult(ResultType.Success, pressure2.Value, "Показатели в норме");
        }
    }
}
