﻿// DataController.cs
namespace DiagnosticSystemService
{
    using System.Web.Http;
    using Models;

    [RoutePrefix("data")]
    public class DataController : ApiController
    {
        [HttpGet]
        [Route("last")]
        public DiagnosticSystemState Last()
        {
            return Manager.Last();
        }

        [HttpGet]
        [Route("create")]
        public bool Create(SensorType type, double value)
        {
            Manager.Update(type, value);
            return true;
        }
    }
}
