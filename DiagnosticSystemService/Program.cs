﻿// Program.cs
namespace DiagnosticSystemService
{
    using Topshelf;

    public class Program
    {
        public static void Main(string[] args)
        {
            HostFactory.Run(x =>
            {
                x.Service<TopShelfService>(s =>
                {
                    s.ConstructUsing(name => new TopShelfService());
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });

                x.RunAsLocalSystem();
                x.SetDescription("Сервер-служба системы диагностики для системы охлаждения");
                x.SetDisplayName("Сервер-служба системы диагностики системы охлаждения климатических камер ЦПИ");
                x.SetServiceName("ClimaticsCoolingDiagnosticService");
            });
        }
    }
}
