﻿// MainWindow.xaml.cs
namespace DiagnosticSystemClient
{
    using System;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using Models;
    using Newtonsoft.Json;
    using static Models.Constants;

    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();

            Settings.SetTitles("Давление до фильтра", "Давление после фильтра", "Температура перед камерами", "Температура после камер", "Температура после градирни");

            Press1.SetParams("Давление до фильтра", Press1Min, Press1Max, Press1OptimalStart, Press1OptimalEnd, Press1MajorDivisionTicks, PressFormatter);
            Press2.SetParams("Давление после фильтра", Press2Min, Press2Max, Press2OptimalStart, Press2OptimalEnd, Press2MajorDivisionTicks, PressFormatter);
            Temp1.SetParams("Температура перед камерами", Temp1Min, Temp1Max, Temp1OptimalStart, Temp1OptimalEnd, Temp1MajorDivisionTicks, TempFormatter);
            Temp2.SetParams("Температура после камер", Temp2Min, Temp2Max, Temp2OptimalStart, Temp2OptimalEnd, Temp2MajorDivisionTicks, TempFormatter);
            Temp3.SetParams("Температура после градирни", Temp3Min, Temp3Max, Temp3OptimalStart, Temp3OptimalEnd, Temp3MajorDivisionTicks, TempFormatter);
        }
        
        private async void OnLoaded(object sender, RoutedEventArgs e)
        {
            var wc = new WebClient { Encoding = Encoding.UTF8 };
            while (true)
            {
                DiagnosticSystemState model;
                try
                {
                    var str = await wc.DownloadStringTaskAsync($"http://{Config.ServerIp}/data/last");
                    model = JsonConvert.DeserializeObject<DiagnosticSystemState>(str);
                }
                catch (Exception exc)
                {
                    var message = exc.GetType() == typeof(WebException)
                        ? "Не удалось связаться с сервером"
                        : exc.GetType() == typeof(JsonException)
                            ? "Не удалось обработать ответ от сервера"
                            : "Непредвиденная ошибка";
                    
                    var sensorDataResult = new SensorDataResult(ResultType.Error, null, message);
                    model = new DiagnosticSystemState
                    {
                        Temp1 = sensorDataResult,
                        Temp2 = sensorDataResult,
                        Temp3 = sensorDataResult,
                        Press1 = sensorDataResult,
                        Press2 = sensorDataResult
                    };
                }

                /*Temp1.SetState(new SensorDataResult(ResultType.Success, 24, "Показатели в норме"));
                Temp2.SetState(new SensorDataResult(ResultType.Success, 29, "Показатели в норме"));
                Temp3.SetState(new SensorDataResult(ResultType.Error, 28, "Низкая эффективность охлаждения"));
                Press1.SetState(new SensorDataResult(ResultType.Success, 4.5, "Показатели в норме"));
                Press2.SetState(new SensorDataResult(ResultType.Error, 3.2, "Засорение фильтра"));

                Settings.Error = SettingsControl.SensorError.Press2 | SettingsControl.SensorError.Temp3;*/

                Temp1.SetState(model.Temp1);
                Temp2.SetState(model.Temp2);
                Temp3.SetState(model.Temp3);
                Press1.SetState(model.Press1);
                Press2.SetState(model.Press2);

                var error = default(SettingsControl.SensorError);
                error = model.Temp1.Type == ResultType.Error ? error | SettingsControl.SensorError.Temp1 : error;
                error = model.Temp2.Type == ResultType.Error ? error | SettingsControl.SensorError.Temp2 : error;
                error = model.Temp3.Type == ResultType.Error ? error | SettingsControl.SensorError.Temp3 : error;
                error = model.Press1.Type == ResultType.Error ? error | SettingsControl.SensorError.Press1 : error;
                error = model.Press2.Type == ResultType.Error ? error | SettingsControl.SensorError.Press2 : error;
                Settings.Error = error;

                await Task.Delay(ClientRequestDelay);
            }
        }
    }
}
