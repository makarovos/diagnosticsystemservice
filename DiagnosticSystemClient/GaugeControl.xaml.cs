﻿// GaugeControl.xaml.cs
namespace DiagnosticSystemClient
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Media;
    using Models;

    public partial class GaugeControl
    {
        private Func<double, string> ValueFormatter;

        private static readonly Dictionary<ResultType, SolidColorBrush> Colors = new Dictionary<ResultType, SolidColorBrush>
        {
            [ResultType.Success] = Brushes.Green, 
            [ResultType.Warning] = Brushes.Blue, 
            [ResultType.Error] = Brushes.Red, 
        };

        public GaugeControl()
        {
            InitializeComponent();
        }

        public void SetParams(string title, double min, double max, double optimalMin, double optimalMax, int majorDivisionsCount, Func<double, string> valueFormatter)
        {
            tbTitle.Text = title;
            gauge.MinValue = min;
            gauge.MaxValue = max;
            gauge.CurrentValue = min;
            gauge.OptimalRangeStartValue= optimalMin;
            gauge.OptimalRangeEndValue = optimalMax;
            gauge.MajorDivisionsCount = majorDivisionsCount;
            ValueFormatter = valueFormatter;
        }

        public void SetState(SensorDataResult result)
        {
            if (result.Value.HasValue)
            {
                if (result.Value.Value > gauge.MaxValue)
                {
                    gauge.CurrentValue = gauge.MaxValue;
                }
                else if (result.Value.Value < gauge.MinValue)
                {
                    gauge.CurrentValue = gauge.MinValue;
                }

                gauge.CurrentValue = result.Value.Value;
            }
            else
            {
                gauge.CurrentValue = gauge.MinValue;
            }

            tbInfo.Foreground = Colors[result.Type];
            tbInfo.Text = result.Message;
            gauge.CurrentValue = result.Value ?? gauge.MinValue;
            gauge.DialText = result.Value.HasValue ? ValueFormatter(result.Value.Value) : null;
        }
    }
}
