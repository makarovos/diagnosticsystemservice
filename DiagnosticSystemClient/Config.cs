﻿// Config.cs
namespace DiagnosticSystemClient
{
    using System.Configuration;
    using System.Runtime.CompilerServices;
    using Models;
    using static System.Configuration.ConfigurationManager;

    public static class Config
    {
        static Config()
        {
            if (ServerIp == null)
            {
                Temperature1SoundEnabled = true;
                Temperature2SoundEnabled = true;
                Temperature3SoundEnabled = true;
                Pressure1SoundEnabled = true;
                Pressure2SoundEnabled = true;
                ServerIp = Constants.ServerIpAddress;
            }
        }
        
        public static string ServerIp
        {
            get => GetString();
            set => SaveString(value);
        }
        
        public static bool Temperature1SoundEnabled
        {
            get => GetBool();
            set => SaveBool(value);
        }

        public static bool Temperature2SoundEnabled
        {
            get => GetBool();
            set => SaveBool(value);
        }

        public static bool Temperature3SoundEnabled
        {
            get => GetBool();
            set => SaveBool(value);
        }

        public static bool Pressure1SoundEnabled
        {
            get => GetBool();
            set => SaveBool(value);
        }

        public static bool Pressure2SoundEnabled
        {
            get => GetBool();
            set => SaveBool(value);
        }

        private static bool GetBool([CallerMemberName] string property = null) => !string.IsNullOrEmpty(AppSettings[property]);

        private static string GetString([CallerMemberName] string property = null) => AppSettings[property];

        private static void SaveBool(bool value, [CallerMemberName] string property = null)
        {
            var config = OpenExeConfiguration(ConfigurationUserLevel.None);
            var newValue = value ? "1" : string.Empty;
            var oldValue = config.AppSettings.Settings[property];
            if (oldValue == null)
            {
                config.AppSettings.Settings.Add(property, newValue);
            }
            else
            {
                oldValue.Value = newValue;
            }

            config.Save(ConfigurationSaveMode.Modified);
            RefreshSection("appSettings");
        }

        private static void SaveString(string value, [CallerMemberName] string property = null)
        {
            var config = OpenExeConfiguration(ConfigurationUserLevel.None);
            var oldValue = config.AppSettings.Settings[property];
            if (oldValue == null)
            {
                config.AppSettings.Settings.Add(property, value);
            }
            else
            {
                oldValue.Value = value;
            }
            config.Save(ConfigurationSaveMode.Modified);
            RefreshSection("appSettings");
        }
    }
}
