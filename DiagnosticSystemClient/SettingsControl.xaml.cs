﻿// SettingsControl.xaml.cs
namespace DiagnosticSystemClient
{
    using System;
    using System.Configuration;
    using System.Net;
    using System.Threading.Tasks;
    using System.Windows;
    using Models;
    using Newtonsoft.Json;

    public partial class SettingsControl
    {
        [Flags]
        public enum SensorError
        {
            Press1 = 1,
            Press2 = 2,
            Temp1 = 4,
            Temp2 = 8,
            Temp3 = 16
        }

        public SettingsControl()
        {
            InitializeComponent();

            SetTbApiAddress(Config.ServerIp);
            
            P1Volume.IsChecked = Config.Pressure1SoundEnabled;
            P2Volume.IsChecked = Config.Pressure2SoundEnabled;
            T1Volume.IsChecked = Config.Temperature1SoundEnabled;
            T2Volume.IsChecked = Config.Temperature2SoundEnabled;
            T3Volume.IsChecked = Config.Temperature3SoundEnabled;

            SetSoundToggle();

            Loaded += RunSound;
            Loaded += RunTemperatureRequests;
        }

        public SensorError Error { private get; set; }

        private void OnSoundClick(object sender, RoutedEventArgs e)
        {
            P1Volume.IsChecked = SoundToggle.IsChecked ?? false;
            P2Volume.IsChecked = SoundToggle.IsChecked ?? false;
            T1Volume.IsChecked = SoundToggle.IsChecked ?? false;
            T2Volume.IsChecked = SoundToggle.IsChecked ?? false;
            T3Volume.IsChecked = SoundToggle.IsChecked ?? false;

            OnP1VolumeClick(null, null);
            OnP2VolumeClick(null, null);
            OnT1VolumeClick(null, null);
            OnT2VolumeClick(null, null);
            OnT3VolumeClick(null, null);
        }

        private void OnP1VolumeClick(object sender, RoutedEventArgs e)
        {
            Config.Pressure1SoundEnabled = P1Volume.IsChecked ?? false;
            SetSoundToggle();
        }

        private void OnP2VolumeClick(object sender, RoutedEventArgs e)
        {
            Config.Pressure2SoundEnabled = P2Volume.IsChecked ?? false;
            SetSoundToggle();
        }

        private void OnT1VolumeClick(object sender, RoutedEventArgs e)
        {
            Config.Temperature1SoundEnabled = T1Volume.IsChecked ?? false;
            SetSoundToggle();
        }

        private void OnT2VolumeClick(object sender, RoutedEventArgs e)
        {
            Config.Temperature2SoundEnabled = T2Volume.IsChecked ?? false;
            SetSoundToggle();
        }

        private void OnT3VolumeClick(object sender, RoutedEventArgs e)
        {
            Config.Temperature3SoundEnabled = T3Volume.IsChecked ?? false;
            SetSoundToggle();
        }

        private void OnEditApiClick(object sender, RoutedEventArgs e)
        {
            tbApi.Visibility = (tglApi.IsChecked ?? false) ? Visibility.Visible : Visibility.Hidden;
            tbIpAddress.Visibility = !(tglApi.IsChecked ?? false) ? Visibility.Visible : Visibility.Hidden;

            if (tglApi.IsChecked ?? true)
            {
                tbApi.Text = Config.ServerIp;
            }
            else
            {
                Config.ServerIp = tbApi.Text;
                SetTbApiAddress(tbApi.Text);
            }
        }

        private async void RunSound(object sender, RoutedEventArgs args)
        {
            while (true)
            {
                if (Error.HasFlag(SensorError.Press1) && Config.Pressure1SoundEnabled
                    || Error.HasFlag(SensorError.Press2) && Config.Pressure2SoundEnabled
                    || Error.HasFlag(SensorError.Temp1) && Config.Temperature1SoundEnabled
                    || Error.HasFlag(SensorError.Temp2) && Config.Temperature2SoundEnabled
                    || Error.HasFlag(SensorError.Temp3) && Config.Temperature3SoundEnabled)
                {
                    Console.Beep(1500, 200);
                }

                await Task.Delay(100);
            }
        }

        private async void RunTemperatureRequests(object sender, RoutedEventArgs args)
        {
            var webClient = new WebClient();
            var api = ConfigurationManager.AppSettings["OpenWeatherMap.Api"];
            var lat = ConfigurationManager.AppSettings["OpenWeatherMap.CoolingTowerLatitude"];
            var lon = ConfigurationManager.AppSettings["OpenWeatherMap.CoolingTowerLongitude"];
            var appId = ConfigurationManager.AppSettings["OpenWeatherMap.APPID"];

            while (true)
            {
                try
                {
                    var str = await webClient.DownloadStringTaskAsync($"{api}?lat={lat}&lon={lon}&APPID={appId}");
                    var temperature = JsonConvert.DeserializeObject<OpenWeatherMapModel>(str).Temp;
                    WeatherTitle.Text = $"Температура на улице: {Constants.TempFormatter(temperature)}";
                    await Task.Delay(1000 * 60 * 10);
                }
                catch
                {
                    WeatherTitle.Text = "Температура на улице: ???";
                    await Task.Delay(1000 * 10);
                }
            }
        }

        private void SetTbApiAddress(string ip)
        {
            tbIpAddress.Text = $"Сервер: {ip} ";
        }

        private void SetSoundToggle()
        {
            SoundToggle.IsChecked = (P1Volume.IsChecked ?? false)
                                    || (P2Volume.IsChecked ?? false)
                                    || (T1Volume.IsChecked ?? false)
                                    || (T2Volume.IsChecked ?? false)
                                    || (T3Volume.IsChecked ?? false);
        }

    private class OpenWeatherMapModel
        {
            private const double KELVIN_OFFSET = 273.15;

            public double Temp => Main.Temp - KELVIN_OFFSET;

            public OpenWeatherMapMain Main { get; set; }

            public class OpenWeatherMapMain
            {
                public double Temp { get; set; }
            }
        }

        public void SetTitles(string p1Title, string p2Title, string t1Title, string t2Title, string t3Title)
        {
            P1Title.Text = $"{p1Title} ";
            P2Title.Text = $"{p2Title} ";
            T1Title.Text = $"{t1Title} ";
            T2Title.Text = $"{t2Title} ";
            T3Title.Text = $"{t3Title} ";
        }
    }
}
