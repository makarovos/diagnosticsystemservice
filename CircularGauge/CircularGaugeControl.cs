﻿// CircularGaugeControl.cs
namespace CircularGauge
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Media.Animation;
    using System.Windows.Shapes;

    [TemplatePart(Name = "LayoutRoot", Type = typeof(Grid))]
    [TemplatePart(Name = "Pointer", Type = typeof(Path))]
    [TemplatePart(Name = "RangeIndicatorLight", Type = typeof(Ellipse))]
    [TemplatePart(Name = "PointerCap", Type = typeof(Ellipse))]
    public class CircularGaugeControl : Control
    {
        public static readonly DependencyProperty CurrentValueProperty =
            DependencyProperty.Register("CurrentValue", typeof(double), typeof(CircularGaugeControl),
                new PropertyMetadata(double.MinValue, OnCurrentValuePropertyChanged));

        public static readonly DependencyProperty MinValueProperty =
            DependencyProperty.Register("MinValue", typeof(double), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty MaxValueProperty =
            DependencyProperty.Register("MaxValue", typeof(double), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty RadiusProperty =
            DependencyProperty.Register("Radius", typeof(double), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty PointerCapRadiusProperty =
            DependencyProperty.Register("PointerCapRadius", typeof(double), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty PointerLengthProperty =
            DependencyProperty.Register("PointerLength", typeof(double), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty ScaleRadiusProperty =
            DependencyProperty.Register("ScaleRadius", typeof(double), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty ScaleStartAngleProperty =
            DependencyProperty.Register("ScaleStartAngle", typeof(double), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty ScaleSweepAngleProperty =
            DependencyProperty.Register("ScaleSweepAngle", typeof(double), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty MajorDivisionsCountProperty =
            DependencyProperty.Register("MajorDivisionsCount", typeof(double), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty MinorDivisionsCountProperty =
            DependencyProperty.Register("MinorDivisionsCount", typeof(double), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty OptimalRangeEndValueProperty =
            DependencyProperty.Register("OptimalRangeEndValue", typeof(double), typeof(CircularGaugeControl),
                new PropertyMetadata(OnOptimalRangeEndValuePropertyChanged));

        public static readonly DependencyProperty OptimalRangeStartValueProperty =
            DependencyProperty.Register("OptimalRangeStartValue", typeof(double), typeof(CircularGaugeControl),
                new PropertyMetadata(OnOptimalRangeStartValuePropertyChanged));

        public static readonly DependencyProperty ImageSourceProperty =
            DependencyProperty.Register("ImageSource", typeof(ImageSource), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty ImageOffsetProperty =
            DependencyProperty.Register("ImageOffset", typeof(double), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty RangeIndicatorLightOffsetProperty =
            DependencyProperty.Register("RangeIndicatorLightOffset", typeof(double), typeof(CircularGaugeControl),
                null);

        public static readonly DependencyProperty ImageSizeProperty =
            DependencyProperty.Register("ImageSize", typeof(Size), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty RangeIndicatorRadiusProperty =
            DependencyProperty.Register("RangeIndicatorRadius", typeof(double), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty RangeIndicatorThicknessProperty =
            DependencyProperty.Register("RangeIndicatorThickness", typeof(double), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty ScaleLabelRadiusProperty =
            DependencyProperty.Register("ScaleLabelRadius", typeof(double), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty ScaleLabelSizeProperty =
            DependencyProperty.Register("ScaleLabelSize", typeof(Size), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty ScaleLabelFontSizeProperty =
            DependencyProperty.Register("ScaleLabelFontSize", typeof(double), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty ScaleLabelForegroundProperty =
            DependencyProperty.Register("ScaleLabelForeground", typeof(Color), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty MajorTickSizeProperty =
            DependencyProperty.Register("MajorTickRectSize", typeof(Size), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty MinorTickSizeProperty =
            DependencyProperty.Register("MinorTickSize", typeof(Size), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty MajorTickColorProperty =
            DependencyProperty.Register("MajorTickColor", typeof(Color), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty MinorTickColorProperty =
            DependencyProperty.Register("MinorTickColor", typeof(Color), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty GaugeBackgroundColorProperty =
            DependencyProperty.Register("GaugeBackgroundColor", typeof(Color), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty PointerThicknessProperty =
            DependencyProperty.Register("PointerThickness", typeof(double), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty ResetPointerOnStartUpProperty =
            DependencyProperty.Register("ResetPointerOnStartUp", typeof(bool), typeof(CircularGaugeControl),
                new PropertyMetadata(false, null));

        public static readonly DependencyProperty ScaleValuePrecisionProperty =
            DependencyProperty.Register("ScaleValuePrecision", typeof(int), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty BelowOptimalRangeColorProperty =
            DependencyProperty.Register("BelowOptimalRangeColor", typeof(Color), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty OptimalRangeColorProperty =
            DependencyProperty.Register("OptimalRangeColor", typeof(Color), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty AboveOptimalRangeColorProperty =
            DependencyProperty.Register("AboveOptimalRangeColor", typeof(Color), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty DialTextProperty =
            DependencyProperty.Register("DialText", typeof(string), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty DialTextColorProperty =
            DependencyProperty.Register("DialTextColor", typeof(Color), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty DialTextFontSizeProperty =
            DependencyProperty.Register("DialTextFontSize", typeof(int), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty DialTextOffsetProperty =
            DependencyProperty.Register("DialTextOffset", typeof(double), typeof(CircularGaugeControl), null);

        public static readonly DependencyProperty RangeIndicatorLightRadiusProperty =
            DependencyProperty.Register("RangeIndicatorLightRadius", typeof(double), typeof(CircularGaugeControl),
                null);

        private readonly int animatingSpeedFactor = 6;
        private double arcradius1;
        private double arcradius2;
        private Ellipse lightIndicator;
        private Path pointer;
        private Ellipse pointerCap;
        private Path rangeIndicator;
        private Grid rootGrid;

        static CircularGaugeControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CircularGaugeControl),
                new FrameworkPropertyMetadata(typeof(CircularGaugeControl)));
        }

        public double CurrentValue
        {
            get => (double) GetValue(CurrentValueProperty);
            set => SetValue(CurrentValueProperty, value);
        }

        public double MinValue
        {
            get => (double) GetValue(MinValueProperty);
            set => SetValue(MinValueProperty, value);
        }

        public double MaxValue
        {
            get => (double) GetValue(MaxValueProperty);
            set => SetValue(MaxValueProperty, value);
        }

        public double Radius
        {
            get => (double) GetValue(RadiusProperty);
            set => SetValue(RadiusProperty, value);
        }

        public double PointerCapRadius
        {
            get => (double) GetValue(PointerCapRadiusProperty);
            set => SetValue(PointerCapRadiusProperty, value);
        }

        public double PointerLength
        {
            get => (double) GetValue(PointerLengthProperty);
            set => SetValue(PointerLengthProperty, value);
        }

        public double PointerThickness
        {
            get => (double) GetValue(PointerThicknessProperty);
            set => SetValue(PointerThicknessProperty, value);
        }

        public double ScaleRadius
        {
            get => (double) GetValue(ScaleRadiusProperty);
            set => SetValue(ScaleRadiusProperty, value);
        }

        public double ScaleStartAngle
        {
            get => (double) GetValue(ScaleStartAngleProperty);
            set => SetValue(ScaleStartAngleProperty, value);
        }

        public double ScaleSweepAngle
        {
            get => (double) GetValue(ScaleSweepAngleProperty);
            set => SetValue(ScaleSweepAngleProperty, value);
        }

        public double MajorDivisionsCount
        {
            get => (double) GetValue(MajorDivisionsCountProperty);
            set => SetValue(MajorDivisionsCountProperty, value);
        }

        public double MinorDivisionsCount
        {
            get => (double) GetValue(MinorDivisionsCountProperty);
            set => SetValue(MinorDivisionsCountProperty, value);
        }

        public double OptimalRangeEndValue
        {
            get => (double) GetValue(OptimalRangeEndValueProperty);
            set => SetValue(OptimalRangeEndValueProperty, value);
        }

        public double OptimalRangeStartValue
        {
            get => (double) GetValue(OptimalRangeStartValueProperty);
            set => SetValue(OptimalRangeStartValueProperty, value);
        }

        public ImageSource ImageSource
        {
            get => (ImageSource) GetValue(ImageSourceProperty);
            set => SetValue(ImageSourceProperty, value);
        }

        public double ImageOffset
        {
            get => (double) GetValue(ImageOffsetProperty);
            set => SetValue(ImageOffsetProperty, value);
        }

        public double RangeIndicatorLightOffset
        {
            get => (double) GetValue(RangeIndicatorLightOffsetProperty);
            set => SetValue(RangeIndicatorLightOffsetProperty, value);
        }

        public Size ImageSize
        {
            get => (Size) GetValue(ImageSizeProperty);
            set => SetValue(ImageSizeProperty, value);
        }

        public double RangeIndicatorRadius
        {
            get => (double) GetValue(RangeIndicatorRadiusProperty);
            set => SetValue(RangeIndicatorRadiusProperty, value);
        }

        public double RangeIndicatorThickness
        {
            get => (double) GetValue(RangeIndicatorThicknessProperty);
            set => SetValue(RangeIndicatorThicknessProperty, value);
        }

        public double ScaleLabelRadius
        {
            get => (double) GetValue(ScaleLabelRadiusProperty);
            set => SetValue(ScaleLabelRadiusProperty, value);
        }

        public Size ScaleLabelSize
        {
            get => (Size) GetValue(ScaleLabelSizeProperty);
            set => SetValue(ScaleLabelSizeProperty, value);
        }

        public double ScaleLabelFontSize
        {
            get => (double) GetValue(ScaleLabelFontSizeProperty);
            set => SetValue(ScaleLabelFontSizeProperty, value);
        }

        public Color ScaleLabelForeground
        {
            get => (Color) GetValue(ScaleLabelForegroundProperty);
            set => SetValue(ScaleLabelForegroundProperty, value);
        }

        public Size MajorTickSize
        {
            get => (Size) GetValue(MajorTickSizeProperty);
            set => SetValue(MajorTickSizeProperty, value);
        }

        public Size MinorTickSize
        {
            get => (Size) GetValue(MinorTickSizeProperty);
            set => SetValue(MinorTickSizeProperty, value);
        }

        public Color MajorTickColor
        {
            get => (Color) GetValue(MajorTickColorProperty);
            set => SetValue(MajorTickColorProperty, value);
        }

        public Color MinorTickColor
        {
            get => (Color) GetValue(MinorTickColorProperty);
            set => SetValue(MinorTickColorProperty, value);
        }

        public Color GaugeBackgroundColor
        {
            get => (Color) GetValue(GaugeBackgroundColorProperty);
            set => SetValue(GaugeBackgroundColorProperty, value);
        }

        public bool ResetPointerOnStartUp
        {
            get => (bool) GetValue(ResetPointerOnStartUpProperty);
            set => SetValue(ResetPointerOnStartUpProperty, value);
        }

        public int ScaleValuePrecision
        {
            get => (int) GetValue(ScaleValuePrecisionProperty);
            set => SetValue(ScaleValuePrecisionProperty, value);
        }

        public Color BelowOptimalRangeColor
        {
            get => (Color) GetValue(BelowOptimalRangeColorProperty);
            set => SetValue(BelowOptimalRangeColorProperty, value);
        }

        public Color OptimalRangeColor
        {
            get => (Color) GetValue(OptimalRangeColorProperty);
            set => SetValue(OptimalRangeColorProperty, value);
        }

        public Color AboveOptimalRangeColor
        {
            get => (Color) GetValue(AboveOptimalRangeColorProperty);
            set => SetValue(AboveOptimalRangeColorProperty, value);
        }

        public string DialText
        {
            get => (string) GetValue(DialTextProperty);
            set => SetValue(DialTextProperty, value);
        }

        public Color DialTextColor
        {
            get => (Color) GetValue(DialTextColorProperty);
            set => SetValue(DialTextColorProperty, value);
        }

        public int DialTextFontSize
        {
            get => (int) GetValue(DialTextFontSizeProperty);
            set => SetValue(DialTextFontSizeProperty, value);
        }

        public double DialTextOffset
        {
            get => (double) GetValue(DialTextOffsetProperty);
            set => SetValue(DialTextOffsetProperty, value);
        }

        public double RangeIndicatorLightRadius
        {
            get => (double) GetValue(RangeIndicatorLightRadiusProperty);
            set => SetValue(RangeIndicatorLightRadiusProperty, value);
        }

        private static void OnCurrentValuePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var gauge = d as CircularGaugeControl;
            gauge?.OnCurrentValueChanged(e);
        }

        private static void OnOptimalRangeEndValuePropertyChanged(DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            var gauge = d as CircularGaugeControl;
            if ((double) e.NewValue > gauge?.MaxValue)
                gauge.OptimalRangeEndValue = gauge.MaxValue;
        }

        private static void OnOptimalRangeStartValuePropertyChanged(DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            var gauge = d as CircularGaugeControl;
            if ((double) e.NewValue < gauge?.MinValue)
                gauge.OptimalRangeStartValue = gauge.MinValue;
        }

        public virtual void OnCurrentValueChanged(DependencyPropertyChangedEventArgs e)
        {
            var newValue = (double) e.NewValue;
            var oldValue = (double) e.OldValue;

            if (newValue > MaxValue)
                newValue = MaxValue;
            else if (newValue < MinValue)
                newValue = MinValue;

            if (oldValue > MaxValue)
                oldValue = MaxValue;
            else if (oldValue < MinValue)
                oldValue = MinValue;

            if (pointer != null)
            {
                var realworldunit = ScaleSweepAngle / (MaxValue - MinValue);

                var oldcurrentvalueAngle = ScaleStartAngle + (oldValue - MinValue) * realworldunit;

                var newcurrentvalueAngle = ScaleStartAngle + (newValue - MinValue) * realworldunit;

                AnimatePointer(oldcurrentvalueAngle, newcurrentvalueAngle);
            }
        }

        private void AnimatePointer(double oldcurrentvalueAngle, double newcurrentvalueAngle)
        {
            if (pointer != null)
            {
                var da = new DoubleAnimation
                {
                    From = oldcurrentvalueAngle,
                    To = newcurrentvalueAngle
                };

                var animDuration = Math.Abs(oldcurrentvalueAngle - newcurrentvalueAngle) * animatingSpeedFactor;
                da.Duration = new Duration(TimeSpan.FromMilliseconds(animDuration));

                var sb = new Storyboard();
                sb.Completed += Sb_Completed;
                sb.Children.Add(da);
                Storyboard.SetTarget(da, pointer);
                Storyboard.SetTargetProperty(da,
                    new PropertyPath("(Path.RenderTransform).(TransformGroup.Children)[0].(RotateTransform.Angle)"));

                if (Math.Abs(newcurrentvalueAngle - oldcurrentvalueAngle) > 0.0001)
                    sb.Begin();
            }
        }

        private void MovePointer(double angleValue)
        {
            if (pointer != null)
            {
                var tg = pointer.RenderTransform as TransformGroup;
                if (tg?.Children[0] is RotateTransform rt)
                    rt.Angle = angleValue;
            }
        }

        private void Sb_Completed(object sender, EventArgs e)
        {
            if (CurrentValue > OptimalRangeEndValue)
                lightIndicator.Fill = GetRangeIndicatorGradEffect(AboveOptimalRangeColor);
            else if (CurrentValue <= OptimalRangeEndValue && CurrentValue >= OptimalRangeStartValue)
                lightIndicator.Fill = GetRangeIndicatorGradEffect(OptimalRangeColor);
            else if (CurrentValue < OptimalRangeStartValue)
                lightIndicator.Fill = GetRangeIndicatorGradEffect(BelowOptimalRangeColor);
        }

        private static GradientBrush GetRangeIndicatorGradEffect(Color gradientColor)
        {
            var gradient = new LinearGradientBrush
            {
                StartPoint = new Point(0, 0),
                EndPoint = new Point(1, 1)
            };

            var color1 = new GradientStop
            {
                Color = gradientColor == Colors.Transparent ? gradientColor : Colors.LightGray,
                Offset = 0.2
            };
            gradient.GradientStops.Add(color1);

            var color2 = new GradientStop
            {
                Color = gradientColor,
                Offset = 0.5
            };
            gradient.GradientStops.Add(color2);

            var color3 = new GradientStop
            {
                Color = gradientColor,
                Offset = 0.8
            };
            gradient.GradientStops.Add(color3);
            return gradient;
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            rootGrid = GetTemplateChild("LayoutRoot") as Grid;
            pointer = GetTemplateChild("Pointer") as Path;
            pointerCap = GetTemplateChild("PointerCap") as Ellipse;
            lightIndicator = GetTemplateChild("RangeIndicatorLight") as Ellipse;

            DrawScale();
            DrawRangeIndicator();

            Panel.SetZIndex(pointer, 100000);
            Panel.SetZIndex(pointerCap, 100001);

            if (ResetPointerOnStartUp)
                MovePointer(ScaleStartAngle);
        }

        private void DrawScale()
        {
            var majorTickUnitAngle = ScaleSweepAngle / MajorDivisionsCount;

            var majorTicksUnitValue = (MaxValue - MinValue) / MajorDivisionsCount;
            majorTicksUnitValue = Math.Round(majorTicksUnitValue, ScaleValuePrecision);

            var minvalue = MinValue;

            for (var i = ScaleStartAngle; i <= ScaleStartAngle + ScaleSweepAngle; i = i + majorTickUnitAngle)
            {
                var majortickrect = new Rectangle
                {
                    Height = MajorTickSize.Height,
                    Width = MajorTickSize.Width,
                    Fill = new SolidColorBrush(MajorTickColor)
                };

                var p = new Point(0.5, 0.5);
                majortickrect.RenderTransformOrigin = p;
                majortickrect.HorizontalAlignment = HorizontalAlignment.Center;
                majortickrect.VerticalAlignment = VerticalAlignment.Center;

                var majortickgp = new TransformGroup();
                var majortickrt = new RotateTransform();

                var i_radian = i * Math.PI / 180;
                majortickrt.Angle = i;
                majortickgp.Children.Add(majortickrt);
                var majorticktt = new TranslateTransform
                {
                    X = (int) (ScaleRadius * Math.Cos(i_radian)),
                    Y = (int) (ScaleRadius * Math.Sin(i_radian))
                };

                var majorscalevaluett = new TranslateTransform
                {
                    X = (int) (ScaleLabelRadius * Math.Cos(i_radian)),
                    Y = (int) (ScaleLabelRadius * Math.Sin(i_radian))
                };

                var tb = new TextBlock
                {
                    Height = ScaleLabelSize.Height,
                    Width = ScaleLabelSize.Width,
                    FontSize = ScaleLabelFontSize,
                    Foreground = new SolidColorBrush(ScaleLabelForeground),
                    TextAlignment = TextAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Center
                };

                if (Math.Round(minvalue, ScaleValuePrecision) <= Math.Round(MaxValue, ScaleValuePrecision))
                {
                    minvalue = Math.Round(minvalue, ScaleValuePrecision);
                    tb.Text = minvalue.ToString(CultureInfo.InvariantCulture);
                    minvalue = minvalue + majorTicksUnitValue;
                }
                else
                {
                    break;
                }

                majortickgp.Children.Add(majorticktt);
                majortickrect.RenderTransform = majortickgp;
                tb.RenderTransform = majorscalevaluett;
                rootGrid.Children.Add(majortickrect);
                rootGrid.Children.Add(tb);

                var onedegree = (i + majorTickUnitAngle - i) / MinorDivisionsCount;

                if (i < ScaleStartAngle + ScaleSweepAngle && Math.Round(minvalue, ScaleValuePrecision) <=
                    Math.Round(MaxValue, ScaleValuePrecision))
                    for (var mi = i + onedegree; mi < i + majorTickUnitAngle; mi = mi + onedegree)
                    {
                        var mr = new Rectangle
                        {
                            Height = MinorTickSize.Height,
                            Width = MinorTickSize.Width,
                            Fill = new SolidColorBrush(MinorTickColor),
                            HorizontalAlignment = HorizontalAlignment.Center,
                            VerticalAlignment = VerticalAlignment.Center
                        };

                        var p1 = new Point(0.5, 0.5);
                        mr.RenderTransformOrigin = p1;

                        var minortickgp = new TransformGroup();
                        var minortickrt = new RotateTransform {Angle = mi};
                        minortickgp.Children.Add(minortickrt);
                        var minorticktt = new TranslateTransform();

                        var mi_radian = mi * Math.PI / 180;
                        minorticktt.X = (int) (ScaleRadius * Math.Cos(mi_radian));
                        minorticktt.Y = (int) (ScaleRadius * Math.Sin(mi_radian));

                        minortickgp.Children.Add(minorticktt);
                        mr.RenderTransform = minortickgp;
                        rootGrid.Children.Add(mr);
                    }
            }
        }

        private Point GetCircumferencePoint(double angle, double radius)
        {
            var angle_radian = angle * Math.PI / 180;
            var X = Radius + radius * Math.Cos(angle_radian);
            var Y = Radius + radius * Math.Sin(angle_radian);
            var p = new Point(X, Y);
            return p;
        }

        private void DrawRangeIndicator()
        {
            var realworldunit = ScaleSweepAngle / (MaxValue - MinValue);

            var optimalStartAngleFromStart = ScaleStartAngle + (OptimalRangeStartValue - MinValue) * realworldunit;

            var optimalEndAngleFromStart = ScaleStartAngle + (OptimalRangeEndValue - MinValue) * realworldunit;

            arcradius1 = RangeIndicatorRadius + RangeIndicatorThickness;
            arcradius2 = RangeIndicatorRadius;

            var endAngle = ScaleStartAngle + ScaleSweepAngle;

            var A = GetCircumferencePoint(ScaleStartAngle, arcradius1);
            var B = GetCircumferencePoint(ScaleStartAngle, arcradius2);
            var C = GetCircumferencePoint(optimalStartAngleFromStart, arcradius2);
            var D = GetCircumferencePoint(optimalStartAngleFromStart, arcradius1);

            var isReflexAngle = Math.Abs(optimalStartAngleFromStart - ScaleStartAngle) > 180.0;
            DrawSegment(A, B, C, D, isReflexAngle, BelowOptimalRangeColor);

            var A1 = GetCircumferencePoint(optimalStartAngleFromStart, arcradius1);
            var B1 = GetCircumferencePoint(optimalStartAngleFromStart, arcradius2);
            var C1 = GetCircumferencePoint(optimalEndAngleFromStart, arcradius2);
            var D1 = GetCircumferencePoint(optimalEndAngleFromStart, arcradius1);
            var isReflexAngle1 = Math.Abs(optimalEndAngleFromStart - optimalStartAngleFromStart) > 180.0;
            DrawSegment(A1, B1, C1, D1, isReflexAngle1, OptimalRangeColor);

            var A2 = GetCircumferencePoint(optimalEndAngleFromStart, arcradius1);
            var B2 = GetCircumferencePoint(optimalEndAngleFromStart, arcradius2);
            var C2 = GetCircumferencePoint(endAngle, arcradius2);
            var D2 = GetCircumferencePoint(endAngle, arcradius1);
            var isReflexAngle2 = Math.Abs(endAngle - optimalEndAngleFromStart) > 180.0;
            DrawSegment(A2, B2, C2, D2, isReflexAngle2, AboveOptimalRangeColor);
        }

        private void DrawSegment(Point p1, Point p2, Point p3, Point p4, bool reflexangle, Color clr)
        {
            var segments = new PathSegmentCollection
            {
                new LineSegment {Point = p2},
                new ArcSegment
                {
                    Size = new Size(arcradius2, arcradius2),
                    Point = p3,
                    SweepDirection = SweepDirection.Clockwise,
                    IsLargeArc = reflexangle
                },
                new LineSegment {Point = p4},
                new ArcSegment
                {
                    Size = new Size(arcradius1, arcradius1),
                    Point = p1,
                    SweepDirection = SweepDirection.Counterclockwise,
                    IsLargeArc = reflexangle
                }
            };

            var rangestrokecolor = clr == Colors.Transparent ? clr : Colors.White;

            rangeIndicator = new Path
            {
                StrokeLineJoin = PenLineJoin.Round,
                Stroke = new SolidColorBrush(rangestrokecolor),
                Fill = new SolidColorBrush(clr),
                Opacity = 0.65,
                StrokeThickness = 0.25,
                Data = new PathGeometry
                {
                    Figures = new PathFigureCollection
                    {
                        new PathFigure
                        {
                            IsClosed = true,
                            StartPoint = p1,
                            Segments = segments
                        }
                    }
                }
            };

            rangeIndicator.SetValue(Panel.ZIndexProperty, 150);
            rootGrid.Children.Add(rangeIndicator);
        }
    }
}