﻿// Constants.cs
namespace Models
{
    using System;

    public static class Constants
    {
        public const string ServerIpAddress = Host + ":" + Port;
        public const string Host = "192.168.1.35";
        public const string Port = "8085";

        public const string Ssid = "ZyXEL_KEENETIC_LITE_43E057";
        public const string SsidPassword = "10031974";
        public const uint MicroControllerDelay = 1000u;
        public const uint MicroControllerServerWaitingDelay = 5000;
        public const int ClientRequestDelay = 2000;

        public const double Temp1Min = 10;
        public const double Temp1Max = 40;
        public const double Temp1OptimalStart = 10;
        public const double Temp1OptimalEnd = 27;
        public const int Temp1MajorDivisionTicks = (int)((Temp1Max - Temp1Min) / 5);
        public const double Temp1MaxWarning = 25;

        public const double Temp2Min = Temp1Min;
        public const double Temp2Max = Temp1Max;
        public const double Temp2OptimalStart = Temp1OptimalStart;
        public const double Temp2OptimalEnd = 32;
        public const int Temp2MajorDivisionTicks = Temp1MajorDivisionTicks;

        public const double Temp3Min = Temp1Min;
        public const double Temp3Max = Temp1Max;
        public const double Temp3OptimalStart = Temp1OptimalStart;
        public const double Temp3OptimalEnd = Temp1OptimalEnd;
        public const int Temp3MajorDivisionTicks = Temp1MajorDivisionTicks;

        public const double Press1Min = 2;
        public const double Press1Max = 6.5;
        public const double Press1OptimalStart = 2.5;
        public const double Press1OptimalEnd = 6;
        public const int Press1MajorDivisionTicks = (int)((Press1Max - Press1Min) / 0.5);
        public const double Press1MaxWarning = 5;
        public const double Press1MinWarning = 3;

        public const double Press2Min = Press1Min;
        public const double Press2Max = Press1Max;
        public const double Press2OptimalStart = Press1OptimalStart;
        public const double Press2OptimalEnd = Press1OptimalEnd;
        public const int Press2MajorDivisionTicks = Press1MajorDivisionTicks;
        public const double Press2MaxWarning = 5;
        public const double Press2MinWarning = 3;

        public const double CoolingEffectiveDifference = 5;
        public const double MaxAllowedDifferenceBetweenPressures = 1;

        public static readonly Func<double, string> TempFormatter = d => $"{d:F0} °C";
        public static readonly Func<double, string> PressFormatter = d => $"{d:F1} bar";
        public static readonly TimeSpan SensorDataExpirationTimeSpan = TimeSpan.FromSeconds(2);
    }
}
