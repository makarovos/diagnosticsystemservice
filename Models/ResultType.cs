﻿// ResultType.cs
namespace Models
{
    public enum ResultType
    {
        Success = 1,
        Warning,
        Error
    }
}
