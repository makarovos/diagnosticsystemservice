﻿// SensorType.cs
namespace Models
{
    public enum SensorType
    {
        Temperature1 = 1,
        Temperature2,
        Temperature3,
        Pressure1,
        Pressure2
    }
}
