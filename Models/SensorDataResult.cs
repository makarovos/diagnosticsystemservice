﻿// SensorDataResult.cs
namespace Models
{
    public class SensorDataResult
    {
        public double? Value { get; set; }

        public string Message { get; set; }

        public ResultType Type { get; set; }

        public SensorDataResult(ResultType type, double? value, string message)
        {
            Value = value;
            Message = message;
            Type = type;
        }
    }
}
