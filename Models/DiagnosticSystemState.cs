﻿// DiagnosticSystemState.cs
namespace Models
{
    public class DiagnosticSystemState
    {
        public SensorDataResult Temp1 { get; set; }
        public SensorDataResult Temp2 { get; set; }
        public SensorDataResult Temp3 { get; set; }
        public SensorDataResult Press1 { get; set; }
        public SensorDataResult Press2 { get; set; }
    }
}
