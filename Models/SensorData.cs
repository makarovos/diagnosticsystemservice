﻿// SensorData.cs
namespace Models
{
    using System;

    public class SensorData
    {
        public DateTime DateTime { get; set; }

        public double Value { get; set; }

        public SensorData(double value)
        {
            DateTime = DateTime.UtcNow;
            Value = value;
        }
    }
}
